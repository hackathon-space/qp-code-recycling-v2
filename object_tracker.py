import time
import random
import numpy as np
from absl import app, flags, logging
from absl.flags import FLAGS
import cv2
import matplotlib.pyplot as plt
import tensorflow as tf
from yolov3_tf2.models import (
    YoloV3, YoloV3Tiny
)
from yolov3_tf2.dataset import transform_images
from yolov3_tf2.utils import draw_outputs, convert_boxes

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from tools import generate_detections as gdet
from PIL import Image


# import serial##mi cambio
import geocoder  # libreria para geolocaclización
from datetime import date  # hora y fecha
from datetime import datetime
import json


import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


flags.DEFINE_string('classes', './data/labels/coco.names',
                    'path to classes file')
flags.DEFINE_string('weights', './weights/yolov3.tf',
                    'path to weights file')
flags.DEFINE_boolean('tiny', False, 'yolov3 or yolov3-tiny')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('video', './data/video/test.mp4',
                    'path to video file or number for webcam)')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'XVID',
                    'codec used in VideoWriter when saving video to file')
flags.DEFINE_integer('num_classes', 80, 'number of classes in the model')
flags.DEFINE_string('class_1', 'bottle', 'Detección de solo una clase')
flags.DEFINE_string('class_2', 'book', 'Detección de solo una clase2')


def main(_argv):

    # Definition of the parameters
    max_cosine_distance = 0.5
    nn_budget = None
    nms_max_overlap = 1

    now = datetime.now()
    fecha = str(now)

    def guardaraa():
        data['proyecto'].append({
            # 'Objeto': label,
            'nombre': 'Vista1',
            'botella': valorbotella,
            'bolsa': valorbolsa,
            'cuaderno': valorcuaderno,
            'carton': valorcarton,
            'mascarilla': valormascarilla,
            'estatico': 0,
            'imagen': 'https://i.imgur.com/1o6mIEW.png',
            'fecha': fecha,
            'latitud': loc1.latlng[0],
            'longitud': loc1.latlng[1],
            "ubicacion": "Peru,Lima,Comas",
            'imagencontaminada1': "https://diariocorreo.pe/resizer/4-0oDjtGiIRpg6B6B4VdkvBWf7g=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/CQIG6DREXZFBHAKISHDZL55KWQ.jpg",
            'imagencontaminada2': "https://diariocorreo.pe/resizer/8fPiEf2oxHRT8NO0m4AmCproZMA=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/THH2F4BJXRGMVC5CMJGTX7C6CY.jpg",
            'imagencontaminada3': "https://portal.andina.pe/EDPfotografia/Thumbnail/2014/01/01/000229333W.jpg",
        })

        with open('data.json', 'w') as file:
            json.dump(data, file, indent=5)

        ref.update({
            'nombre': 'Vista1',
            'botella': valorbotella,
            'bolsa': valorbolsa,
            'cuaderno': valorcuaderno,
            'carton': valorcarton,
            'mascarilla': valormascarilla,
            'estatico': 0,
            'imagen': 'https://i.imgur.com/1o6mIEW.png',
            'fecha': fecha,
            'latitud': loc1.latlng[0],
            'longitud': loc1.latlng[1],
            "ubicacion": "Peru,Lima,Comas",
            'imagencontaminada1': "https://diariocorreo.pe/resizer/4-0oDjtGiIRpg6B6B4VdkvBWf7g=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/CQIG6DREXZFBHAKISHDZL55KWQ.jpg",
            'imagencontaminada2': "https://diariocorreo.pe/resizer/8fPiEf2oxHRT8NO0m4AmCproZMA=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/THH2F4BJXRGMVC5CMJGTX7C6CY.jpg",
            'imagencontaminada3': "https://portal.andina.pe/EDPfotografia/Thumbnail/2014/01/01/000229333W.jpg", })

    # initialize deep sort
    model_filename = 'model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    metric = nn_matching.NearestNeighborDistanceMetric(
        "cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric, max_age=40)

    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)

    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=FLAGS.num_classes)
    else:
        yolo = YoloV3(classes=FLAGS.num_classes)

    yolo.load_weights(FLAGS.weights)
    logging.info('weights loaded')

    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]
    logging.info('classes loaded')

    try:
        vid = cv2.VideoCapture(int(FLAGS.video))
    except:
        vid = cv2.VideoCapture(FLAGS.video)

    out = None

    if FLAGS.output:
        # by default VideoCapture returns float instead of int
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(vid.get(cv2.CAP_PROP_FPS))
        codec = cv2.VideoWriter_fourcc(*FLAGS.output_format)
        out = cv2.VideoWriter(FLAGS.output, codec, fps, (width, height))
        list_file = open('detection.txt', 'w')
        frame_index = -1

    fps = 0.0
    count = 0
    while True:
        _, img = vid.read()

        if img is None:
            logging.warning("Empty Frame")
            time.sleep(0.1)
            count += 1
            if count < 3:
                continue
            else:
                break

        img_in = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_in = tf.expand_dims(img_in, 0)
        img_in = transform_images(img_in, FLAGS.size)

        t1 = time.time()
        boxes, scores, classes, nums = yolo.predict(img_in)
        classes = classes[0]
        names = []
        for i in range(len(classes)):
            names.append(class_names[int(classes[i])])
        names = np.array(names)
        converted_boxes = convert_boxes(img, boxes[0])
        features = encoder(img, converted_boxes)
        detections = [Detection(bbox, score, class_name, feature) for bbox, score,
                      class_name, feature in zip(converted_boxes, scores[0], names, features)]

        # initialize color map
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

        # run non-maxima suppresion
        boxs = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        classes = np.array([d.class_name for d in detections])
        indices = preprocessing.non_max_suppression(
            boxs, classes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]

        # Call the tracker
        tracker.predict()
        tracker.update(detections)

        valorbotella = 0
        valorcuaderno = 0
        valorbolsa = 0
        valorcarton = 0
        valormascarilla = 0

        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue

            bbox = track.to_tlbr()
            class_name = track.get_class()
            class_name2 = track.get_class()
            if(FLAGS.class_1 == 'bottle'):
                guardaraa()
                valorbotella += 1
                print(FLAGS.class_1)
                print(f'{valorbotella} a')
                color = colors[int(track.track_id) % len(colors)]
                color = [i * 255 for i in color]
                cv2.rectangle(img, (int(bbox[0]), int(
                    bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
                cv2.rectangle(img, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(
                    len(class_name)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)
                cv2.putText(img, class_name + "-" + str(track.track_id),
                            (int(bbox[0]), int(bbox[1]-10)), 0, 0.75, (255, 255, 255), 2)

            if(class_name == 'book'):
                valorcuaderno += 1
                print(FLAGS.class_2)
                print(f'{valorcuaderno} b')
                color = colors[int(track.track_id) % len(colors)]
                color = [i * 255 for i in color]
                cv2.rectangle(img, (int(bbox[0]), int(
                    bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
                cv2.rectangle(img, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(
                    len(class_name2)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)
                cv2.putText(img, class_name2,
                            (int(bbox[0]), int(bbox[1]-10)), 0, 0.75, (255, 255, 255), 2)

            # elif(FLAGS.class_1 != 'person'):
            #     if(class_name == FLAGS.class_1):
            #         objects1 += 1

            #         print(f'{objects3} c')
            #         color = colors[int(track.track_id) % len(colors)]
            #         color = [i * 255 for i in color]
            #         cv2.rectangle(img, (int(bbox[0]), int(
            #             bbox[1])), (int(bbox[2]), int(bbox[3])), color, 2)
            #         cv2.rectangle(img, (int(bbox[0]), int(bbox[1]-30)), (int(bbox[0])+(
            #             len(class_name)+len(str(track.track_id)))*17, int(bbox[1])), color, -1)
            #         cv2.putText(img, class_name + "-" + str(track.track_id),
            #                     (int(bbox[0]), int(bbox[1]-10)), 0, 0.75, (255, 255, 255), 2)

        # IMPRIME EL TEXTO(SE PUEDE MODIFICAR EN FONT Y COLORES)

       # print("Objetos filtrados:{}".format(objects))
        # print N_objects on screen
        if valorbotella > 0:
            botella = valorbotella-valorcuaderno-valorbolsa-valorcarton-valormascarilla
        else:
            botella = 0

        cuaderno = valorcuaderno-valorbolsa-valorcarton-valormascarilla

        cv2.putText(img, "# Botellas: {}".format(botella), (0, 30),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (27, 181, 255), 2
                    )

        cv2.putText(img, "# Cuadernos: {}".format(cuaderno), (300, 30),
                    cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (27, 181, 255), 2)
        # UNCOMMENT BELOW IF YOU WANT CONSTANTLY CHANGING YOLO DETECTIONS TO BE SHOWN ON SCREEN

        # for det in detections:
        #     bbox = det.to_tlbr()
        #     cv2.rectangle(img, (int(bbox[0]), int(bbox[1])), (int(
        #         bbox[2]), int(bbox[3])), (255, 0, 0), 2)

        # print fps on screen
        #fps  = ( fps + (1./(time.time()-t1)) ) / 2
        # cv2.putText(img, "FPS: {:.2f}".format(fps), (0, 30),
        #                  cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 2)
        cv2.namedWindow('output', cv2.WINDOW_NORMAL)
        cv2.setWindowProperty(
            'output', cv2.WND_PROP_FULLSCREEN-5, cv2.WINDOW_FULLSCREEN)
        cv2.imshow('output', img)
        if FLAGS.output:
            out.write(img)
            frame_index = frame_index + 1
            list_file.write(str(frame_index)+' ')
            if len(converted_boxes) != 0:
                for i in range(0, len(converted_boxes)):
                    list_file.write(str(converted_boxes[i][0]) + ' '+str(converted_boxes[i][1]) + ' '+str(
                        converted_boxes[i][2]) + ' '+str(converted_boxes[i][3]) + ' ')
            list_file.write('\n')

        # press q to quit
        if cv2.waitKey(1) == ord('q'):
            break
    vid.release()
    if FLAGS.output:
        out.release()
        list_file.close()
    cv2.destroyAllWindows()


if __name__ == '__main__':

    loc1 = geocoder.osm('Parque Nacional Huascarán, Ancash, Peru')
    hoy = date.today()
    data = {}
    data['proyecto'] = []

    firebase_sdk = credentials.Certificate(
        'fir-space-6a979-firebase-adminsdk-9uyc3-182178af65.json')

    firebase_admin.initialize_app(firebase_sdk, {
        'databaseURL': 'https://fir-space-6a979-default-rtdb.firebaseio.com/'})

    ref = db.reference('/Basegeneral/Vista1')

    try:
        app.run(main)
    except SystemExit:
        pass
